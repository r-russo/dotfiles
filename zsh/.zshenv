typeset -U path
export EDITOR=nvim
export PATH="$HOME/.local/bin:$PATH"

if [ -d "$HOME/.platformio/" ]; then
	export PATH="$PATH:$HOME/.platformio/penv/bin"
fi

if [ -d "$HOME/Android" ]; then
	export ANDROID_HOME=$HOME/Android/Sdk
	export PATH=$PATH:$ANDROID_HOME/emulator
	export PATH=$PATH:$ANDROID_HOME/platform-tools
	export PATH=$PATH:$ANDROID_HOME/../flutter/bin
fi
