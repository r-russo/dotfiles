vim.keymap.set('n', 'gh', '<cmd>ClangdSwitchSourceHeader<cr>', { desc = '[G]oto [H]eader' })
vim.o.shiftwidth = 2
vim.o.tabstop = 2
