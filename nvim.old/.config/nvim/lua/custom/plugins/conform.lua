return {
	'stevearc/conform.nvim',
	opts = {
		formatters_by_ft = {
			lua = { "stylua" },
			python = { { "isort", "ruff_format" } },
			javascript = { "prettierd" },
			typescript = { "prettierd" },
			html = { "prettierd" },
			css = { "prettierd" },
			c = { "clang_format" },
			cpp = { "clang_format" },
			arduino = { "clang_format" },
			cmake = { "cmake_format" },
			latex = { "latexindent" },
		},
		format_on_save = {
			timeout_ms = 500,
			lsp_fallback = true,
		},
	},
}
