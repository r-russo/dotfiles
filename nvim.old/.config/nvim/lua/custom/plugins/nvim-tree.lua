return {
	'nvim-tree/nvim-tree.lua',
	version = "*",
	lazy = false,
	dependencies = 'nvim-tree/nvim-web-devicons',
	opts = {
		sort = {
			sorter = "case_sensitive",
		},
		view = {
			width = 30,
		},
		renderer = {
			group_empty = true,
		},
		diagnostics = {
			enable = true,
		},
		modified = {
			enable = true,
		},
		filters = {
			dotfiles = true,
		},
	},
	keys = {
		{ "<leader>n", "<cmd>NvimTreeToggle<cr>", desc = "NvimTree" },
	},
}
