return {
	"akinsho/flutter-tools.nvim",
	lazy = false,
	dependencies = {
		"nvim-lua/plenary.nvim",
		"stevearc/dressing.nvim", -- optional for vim.ui.select
	},
	opts = {
		widget_guides = {
			enabled = true,
		},
		lsp = {
			on_attach = require("lsp").on_attach,
		},
		dev_log = {
			enabled = true,
			notify_errors = false, -- if there is an error whilst running then notify the user
			open_cmd = "botright split", -- command to use to open the log buffer
		},
	},
}
