return {
	"lervag/vimtex",
	lazy = false,
	init = function()
		vim.g.vimtex_compiler_latexmk = {
			aux_dir = "aux",
			-- out_dir = "build",
			options = {
				"-shell-escape",
				"-verbose",
				"-file-line-error",
				"-interaction=nonstopmode",
				"-synctex=1",
			},
		}
		vim.g.vimtex_view_method = "zathura"
	end
}
