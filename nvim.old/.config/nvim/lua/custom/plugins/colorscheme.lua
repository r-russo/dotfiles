-- {
--   "catppuccin/nvim",
--   name = "catppuccin",
--   priority = 1000,
--   config = function()
--     require("catppuccin").setup({
--       integrations = { fidget = true, neotree = true }
--     })
--     vim.cmd.colorscheme "catppuccin-mocha"
--   end,
-- },
return {
	"EdenEast/nightfox.nvim",
	opts = {
		options = {
			styles = {
				comments = "italic",
				keywords = "bold",
				types = "italic",
			}
		}
	},
	cmd = function()
		vim.cmd("colorscheme nightfox")
	end
}
