return {
	'Civitasv/cmake-tools.nvim',
	dependencies = "akinsho/toggleterm.nvim",
	opts = {
		cmake_build_directory = "build/${variant:buildType}",
		cmake_executor = { -- executor to use
			name = "quickfix", -- name of the executor
			opts = {}, -- the options the executor will get, possible values depend on the executor type. See `default_opts` for possible values.
			default_opts = { -- a list of default and possible values for executors
				quickfix = {
					show = "always",
					position = "belowright",
					size = 10,
					encoding = "utf-8",
					auto_close_when_success = true,
				},
			}
		},
		cmake_runner = {
			name = "toggleterm",
			opts = {},
			default_opts = {
				toggleterm = {
					direction = "horizontal",
					close_on_exit = false,
					auto_scroll = true,
				},
			},
		},
		cmake_dap_configuration = { -- debug settings for cmake
			name = "cpp",
			type = "cppdbg",
			request = "launch",
			stopOnEntry = false,
			runInTerminal = true,
			console = "integratedTerminal",
		},
	}
}
