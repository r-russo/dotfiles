local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi

local gears = require("gears")
local themes_path = gears.filesystem.get_themes_dir()
local conf_dir = gears.filesystem.get_configuration_dir()

local colors = {
    Rosewater = "#f5e0dc",
    Flamingo = "#f2cdcd",
    Pink = "#f5c2e7",
    Mauve = "#cba6f7",
    Red = "#f38ba8",
    Maroon = "#eba0ac",
    Peach = "#fab387",
    Yellow = "#f9e2af",
    Green = "#a6e3a1",
    Teal = "#94e2d5",
    Sky = "#89dceb",
    Sapphire = "#74c7ec",
    Blue = "#89b4fa",
    Lavender = "#b4befe",
    Text = "#cdd6f4",
    Subtext1 = "#bac2de",
    Subtext0 = "#a6adc8",
    Overlay2 = "#9399b2",
    Overlay1 = "#7f849c",
    Overlay0 = "#6c7086",
    Surface2 = "#585b70",
    Surface1 = "#45475a",
    Surface0 = "#313244",
    Base = "#1e1e2e",
    Mantle = "#181825",
    Crust = "#11111b",
}

local theme = {}

theme.font = "Nunito Regular 11"

theme.bg_normal = colors.Base
theme.bg_focus = colors.Blue
theme.bg_urgent = colors.Red
theme.bg_minimize = colors.Overlay1
theme.bg_systray = colors.Base
theme.titlebar_bg_focus = colors.Base
theme.titlebar_fg_focus = colors.Text

theme.fg_normal = colors.Text
theme.fg_focus = colors.Base
theme.fg_urgent = colors.Base
theme.fg_minimize = colors.Base

theme.useless_gap = dpi(5)
theme.border_width = dpi(0)
theme.border_normal = colors.Surface1
theme.border_focus = colors.Blue
theme.border_marked = colors.Green

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(taglist_square_size, theme.fg_normal)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(taglist_square_size, theme.fg_normal)

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]
theme.notification_font = "Nunito Medium 14"
theme.notification_bg = colors.Base
theme.notification_fg = colors.Text
theme.notification_shape = gears.shape.rounded_rect
theme.notification_border_width = dpi(40)
theme.notification_border_color = colors.Green
theme.notification_opacity = 0.8
theme.notification_margin = dpi(32)
theme.notification_width = dpi(600)
theme.notification_height = dpi(100)
theme.notification_max_width = dpi(600)
theme.notification_max_height = dpi(400)
theme.notification_icon_size = dpi(128)

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = themes_path .. "default/submenu.png"
theme.menu_bg_normal = colors.Base
theme.menu_bg_focus = colors.Surface1
theme.menu_fg_normal = colors.Text
theme.menu_fg_focus = colors.Text
theme.menu_height = dpi(32)
theme.menu_width = dpi(200)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_height = dpi(24)
theme.titlebar_close_button_normal = conf_dir .. "icons/window-close.png"
theme.titlebar_close_button_focus = conf_dir .. "icons/window-close.png"

theme.titlebar_minimize_button_normal = conf_dir .. "icons/window-minimize.png"
theme.titlebar_minimize_button_focus = conf_dir .. "./icons/window-minimize.png"

theme.titlebar_ontop_button_normal_inactive = themes_path .. "default/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive = themes_path .. "default/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = themes_path .. "default/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active = themes_path .. "default/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = themes_path .. "default/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive = themes_path .. "default/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = themes_path .. "default/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active = themes_path .. "default/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = themes_path .. "default/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive = themes_path .. "default/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = themes_path .. "default/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active = themes_path .. "default/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = conf_dir .. "icons/window-maximize.png"
theme.titlebar_maximized_button_focus_inactive = conf_dir .. "icons/window-maximize.png"
theme.titlebar_maximized_button_normal_active = conf_dir .. "icons/window-maximize.png"
theme.titlebar_maximized_button_focus_active = conf_dir .. "icons/window-maximize.png"

theme.wallpaper = themes_path .. "default/background.png"

-- You can use your own layout icons like this:
theme.layout_fairh = themes_path .. "default/layouts/fairhw.png"
theme.layout_fairv = themes_path .. "default/layouts/fairvw.png"
theme.layout_floating = themes_path .. "default/layouts/floatingw.png"
theme.layout_magnifier = themes_path .. "default/layouts/magnifierw.png"
theme.layout_max = themes_path .. "default/layouts/maxw.png"
theme.layout_fullscreen = themes_path .. "default/layouts/fullscreenw.png"
theme.layout_tilebottom = themes_path .. "default/layouts/tilebottomw.png"
theme.layout_tileleft = themes_path .. "default/layouts/tileleftw.png"
theme.layout_tile = themes_path .. "default/layouts/tilew.png"
theme.layout_tiletop = themes_path .. "default/layouts/tiletopw.png"
theme.layout_spiral = themes_path .. "default/layouts/spiralw.png"
theme.layout_dwindle = themes_path .. "default/layouts/dwindlew.png"
theme.layout_cornernw = themes_path .. "default/layouts/cornernww.png"
theme.layout_cornerne = themes_path .. "default/layouts/cornernew.png"
theme.layout_cornersw = themes_path .. "default/layouts/cornersww.png"
theme.layout_cornerse = themes_path .. "default/layouts/cornersew.png"

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(theme.menu_height, theme.bg_focus, theme.fg_focus)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = "/home/russo/.local/share/icons/Tela-dark"

return theme
