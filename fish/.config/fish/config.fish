if status is-interactive
    # Commands to run in interactive sessions can go here
end

set -g fish_greeting

alias di='sudo dnf install'
alias ds='dnf search'
alias dr='sudo dnf remove'

alias dots='cd ~/dotfiles'
alias v='nvim'

alias gu='git add . && git commit -m "update" && git push'
alias s="TERM=xterm-256color ssh"

fish_add_path ~/.local/bin/

zoxide init --cmd cd fish | source
starship init fish | source
