function ltaaid --wraps='exa $EXA_STANDARD_OPTIONS $EXA_LAAID_OPTIONS $EXA_LT_OPTIONS' --wraps='eza $EZA_STANDARD_OPTIONS $EZA_LAAID_OPTIONS $EZA_LT_OPTIONS' --description 'alias ltaaid eza $EZA_STANDARD_OPTIONS $EZA_LAAID_OPTIONS $EZA_LT_OPTIONS'
  eza $EZA_STANDARD_OPTIONS $EZA_LAAID_OPTIONS $EZA_LT_OPTIONS $argv
        
end
