function lo --wraps='exa $EXA_STANDARD_OPTIONS $EXA_LO_OPTIONS' --wraps='eza $EZA_STANDARD_OPTIONS $EZA_LO_OPTIONS' --description 'alias lo eza $EZA_STANDARD_OPTIONS $EZA_LO_OPTIONS'
  eza $EZA_STANDARD_OPTIONS $EZA_LO_OPTIONS $argv
        
end
