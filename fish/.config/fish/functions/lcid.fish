function lcid --wraps='exa $EXA_STANDARD_OPTIONS $EXA_LID_OPTIONS $EXA_LC_OPTIONS' --wraps='eza $EZA_STANDARD_OPTIONS $EZA_LID_OPTIONS $EZA_LC_OPTIONS' --description 'alias lcid eza $EZA_STANDARD_OPTIONS $EZA_LID_OPTIONS $EZA_LC_OPTIONS'
  eza $EZA_STANDARD_OPTIONS $EZA_LID_OPTIONS $EZA_LC_OPTIONS $argv
        
end
