function laai --wraps='exa $EXA_STANDARD_OPTIONS $EXA_LAAI_OPTIONS $EXA_L_OPTIONS' --wraps='eza $EZA_STANDARD_OPTIONS $EZA_LAAI_OPTIONS $EZA_L_OPTIONS' --description 'alias laai eza $EZA_STANDARD_OPTIONS $EZA_LAAI_OPTIONS $EZA_L_OPTIONS'
  eza $EZA_STANDARD_OPTIONS $EZA_LAAI_OPTIONS $EZA_L_OPTIONS $argv
        
end
