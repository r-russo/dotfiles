function lgai --wraps='exa $EXA_STANDARD_OPTIONS $EXA_LAI_OPTIONS $EXA_LG_OPTIONS' --wraps='eza $EZA_STANDARD_OPTIONS $EZA_LAI_OPTIONS $EZA_LG_OPTIONS' --description 'alias lgai eza $EZA_STANDARD_OPTIONS $EZA_LAI_OPTIONS $EZA_LG_OPTIONS'
  eza $EZA_STANDARD_OPTIONS $EZA_LAI_OPTIONS $EZA_LG_OPTIONS $argv
        
end
