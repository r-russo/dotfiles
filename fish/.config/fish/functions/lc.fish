function lc --wraps='exa $EXA_STANDARD_OPTIONS $EXA_LC_OPTIONS' --wraps='eza $EZA_STANDARD_OPTIONS $EZA_LC_OPTIONS' --description 'alias lc eza $EZA_STANDARD_OPTIONS $EZA_LC_OPTIONS'
  eza $EZA_STANDARD_OPTIONS $EZA_LC_OPTIONS $argv
        
end
