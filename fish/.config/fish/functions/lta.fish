function lta --wraps='exa $EXA_STANDARD_OPTIONS $EXA_LA_OPTIONS $EXA_LT_OPTIONS' --wraps='eza $EZA_STANDARD_OPTIONS $EZA_LA_OPTIONS $EZA_LT_OPTIONS' --description 'alias lta eza $EZA_STANDARD_OPTIONS $EZA_LA_OPTIONS $EZA_LT_OPTIONS'
  eza $EZA_STANDARD_OPTIONS $EZA_LA_OPTIONS $EZA_LT_OPTIONS $argv
        
end
