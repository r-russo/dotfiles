function lg --wraps='exa $EXA_STANDARD_OPTIONS $EXA_LG_OPTIONS' --wraps='eza $EZA_STANDARD_OPTIONS $EZA_LG_OPTIONS' --description 'alias lg eza $EZA_STANDARD_OPTIONS $EZA_LG_OPTIONS'
  eza $EZA_STANDARD_OPTIONS $EZA_LG_OPTIONS $argv
        
end
