function loid --wraps='exa $EXA_STANDARD_OPTIONS $EXA_LID_OPTIONS $EXA_LO_OPTIONS' --wraps='eza $EZA_STANDARD_OPTIONS $EZA_LID_OPTIONS $EZA_LO_OPTIONS' --description 'alias loid eza $EZA_STANDARD_OPTIONS $EZA_LID_OPTIONS $EZA_LO_OPTIONS'
  eza $EZA_STANDARD_OPTIONS $EZA_LID_OPTIONS $EZA_LO_OPTIONS $argv
        
end
