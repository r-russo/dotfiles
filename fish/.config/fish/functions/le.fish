function le --wraps='exa $EXA_STANDARD_OPTIONS $EXA_LE_OPTIONS' --wraps='eza $EZA_STANDARD_OPTIONS $EZA_LE_OPTIONS' --description 'alias le eza $EZA_STANDARD_OPTIONS $EZA_LE_OPTIONS'
  eza $EZA_STANDARD_OPTIONS $EZA_LE_OPTIONS $argv
        
end
