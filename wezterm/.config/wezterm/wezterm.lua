local wezterm = require("wezterm")

local config = wezterm.config_builder()

config.initial_rows = 30
config.initial_cols = 90
config.font_size = 13
config.font = wezterm.font("JetBrainsMono")

config.color_scheme = "Catppuccin Mocha"
config.audible_bell = "Disabled"
config.hide_tab_bar_if_only_one_tab = true

config.underline_position = -3

config.enable_wayland = false
config.enable_scroll_bar = true
-- config.window_decorations = "INTEGRATED_BUTTONS|RESIZE"

return config
